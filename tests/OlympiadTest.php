<?php

use PHPUnit\Framework\TestCase;

class OlympiadTest extends \PHPUnit\Framework\TestCase
{

    private $comp;

    protected function setUp(): void
    {
        $this->comp = new App\Competition();
    }

    protected function tearDown(): void
    {
        isset($this->comp);
    }

    public function test_max_participants(){
        $result = $this->comp->grt_olympiad_max_participants();
        $this->assertEquals(30, $result);
    }

    /**
     * *@dataProvider newDataProvider
     * @param $a
     * @param $expected
     * @throws \PHPUnit\Framework\ExpectationFailedException
     * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
     */
    public function test_get_city_by_year($a, $expected){
        $result = $this->comp->get_city_by_year($a);
        $this->assertEquals($expected, $result);
    }

    public function newDataProvider(){
        return array(
            array('2000', 'Barcelona'),
            array('2016', 'London'),
            array('2004', 'Sydney')
        );
    }
}