<?php


namespace App;

include 'Olympiad.php';

class Competition
{
    private $olympiads;

    public function __construct()
    {
        $this->olympiads = array(
            new Olympiad('Barcelona', '2000', 42),
            new Olympiad('London', '2016', 46),
            new Olympiad('Sydney', '2004', 30),
        ) ;
    }

    public function get_city_by_year($year){

        foreach ($this->olympiads as $olympiad){
            if($olympiad->getYear() == $year){
                return $olympiad->getCity();
            }
        }
    }

    public function grt_olympiad_max_participants(){
        foreach ($this->olympiads as $olympiad){
            max($this->olympiads);
        }
        return $olympiad->getParticipants();
    }
}