<?php


namespace App;


class Olympiad
{
    private $city;
    private $year;
    private $participants;

    /**
     * Olympiad constructor.
     * @param $city
     * @param $year
     * @param $participants
     */
    public function __construct($city, $year, $participants)
    {
        $this->city = $city;
        $this->year = $year;
        $this->participants = $participants;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * @return mixed
     */
    public function getParticipants()
    {
        return $this->participants;
    }

    /**
     * @param mixed $participants
     */
    public function setParticipants($participants)
    {
        $this->participants = $participants;
    }

}